const validator = require('validator')

const input = function () {
  return assert.data[assert.prop]
}

const assert = function (data) {
  assert.error = false
  assert.hasError = false
  assert.inputName = ''
  assert.errors = []
  assert.data = data
  return assert
}

const assertFunctions = {

  stopOnFirstError: true,
  error: false,
  hasError: false,
  inputName: '',
  errors: [],
  data: {},

  object: assert,

  _input: function () {
    return input()
  },

  the: function (prop, fn, args) {
    if (this.stopOnError()) return this

    this.prop = prop

    if (fn) {
      // apply assert function
      if (typeof fn === 'string') assert[fn](args)

      // custom function
      if (typeof fn === 'function') {
        if (!fn(input(), args)) this.hasError = true
      }
    }

    return this
  },

  or: function (msg) {
    if (this.stopOnError()) return this
    if (this.hasError) {
      // push the errors
      this.errors.push({
        input: this.prop,
        message: msg
      })

      // just the fist error
      if (!this.error) {
        this.inputName = this.prop
        this.error = new Error(msg)
      }
    }
    return this
  },

  stopOnError: function (x) {
    return this.stopOnFirstError && (this.error !== false)
  },

  // check if the string contains the seed.
  contains: function (seed) {
    if (this.stopOnError()) return this
    if (!validator.contains(input(), seed)) this.hasError = true
    return this
  },

  // check if the string matches the comparison.
  equals: function (comparison) {
    if (this.stopOnError()) return this
    if (!validator.equals(input(), comparison)) this.hasError = true
    return this
  },

  // check if the string is a date that's after the specified date (defaults to now).
  isAfter: function (date) {
    if (this.stopOnError()) return this
    if (!validator.isAfter(input(), date)) this.hasError = true
    return this
  },

  // check if the string contains only letters (a-zA-Z).
  isAlpha: function (locale) {
    if (this.stopOnError()) return this
    if (!validator.isAlpha(input(), locale)) this.hasError = true
    return this
  },

  // check if the string contains only letters and numbers.
  isAlphanumeric: function (locale) {
    if (this.stopOnError()) return this
    if (!validator.isAlphanumeric(input(), locale)) this.hasError = true
    return this
  },

  // check if the string contains ASCII chars only.
  isAscii: function () {
    if (this.stopOnError()) return this
    if (!validator.isAscii(input())) this.hasError = true
    return this
  },

  // check if a string is base64 encoded.
  isBase64: function () {
    if (this.stopOnError()) return this
    if (!validator.isBase64(input())) this.hasError = true
    return this
  },

  // check if the string is a date that's before the specified date.
  isBefore: function (date) {
    if (this.stopOnError()) return this
    if (!validator.isBefore(input(), date)) this.hasError = true
    return this
  },

  // check if the string's length (in UTF-8 bytes) falls in a range.
  isByteLength: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isByteLength(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a credit card.
  isCreditCard: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isCreditCard(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a valid currency amount.
  isCurrency: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isCurrency(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a data uri format.
  isDataURI: function() {
    if (this.stopOnError()) return this
    if (!validator.isDataURI(input())) this.hasError = true
    return this
  },

  // check if the string represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc.
  isDecimal: function(options) {
    if (this.stopOnError()) return this
    if (!validator.isDecimal(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a number that's divisible by another.
  isDivisibleBy: function(number) {
    if (this.stopOnError()) return this
    if (!validator.isDivisibleBy(input(), number)) this.hasError = true
    return this
  },

  // check if the string is an email.
  isEmail: function(options) {
    if (this.stopOnError()) return this
    if (!validator.isEmail(input(), options)) this.hasError = true
    return this
  },

  // check if the string has a length of zero.
  isEmpty: function () {
    if (this.stopOnError()) return this
    if (!validator.isEmpty(input())) this.hasError = true
    return this
  },

  // check if the string is a fully qualified domain name (e.g. domain.com).
  isFQDN: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isFQDN(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a float.
  isFloat: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isFloat(String(input()), options)) this.hasError = true
    return this
  },

  // check if the string contains any full-width chars.
  isFullWidth: function () {
    if (this.stopOnError()) return this
    if (!validator.isFullWidth(input())) this.hasError = true
    return this
  },

  // check if the string contains any half-width chars.
  isHalfWidth: function () {
    if (this.stopOnError()) return this
    if (!validator.isHalfWidth(input())) this.hasError = true
    return this
  },

  // check if the string is a hash of type algorithm.
  isHash: function (algorithm) {
    if (this.stopOnError()) return this
    if (!validator.isHash(input(), algorithm)) this.hasError = true
    return this
  },

  // check if the string is a hexadecimal color.
  isHexColor: function () {
    if (this.stopOnError()) return this
    if (!validator.isHexColor(input())) this.hasError = true
    return this
  },

  // check if the string is a hexadecimal number.
  isHexadecimal: function () {
    if (this.stopOnError()) return this
    if (!validator.isHexadecimal(input())) this.hasError = true
    return this
  },

  // check if the string is an IP (version 4 or 6).
  isIP: function (version) {
    if (this.stopOnError()) return this
    if (!validator.isIP(input(), version)) this.hasError = true
    return this
  },

  // check if the string is an ISBN (version 10 or 13).
  isISBN: function (version) {
    if (this.stopOnError()) return this
    if (!validator.isISBN(input(), version)) this.hasError = true
    return this
  },

  // check if the string is an ISSN.
  isISSN: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isISSN(input(), options)) this.hasError = true
    return this
  },

  // check if the string is an ISSN.
  isISIN: function () {
    if (this.stopOnError()) return this
    if (!validator.isISIN(input())) this.hasError = true
    return this
  },

  // check if the string is a valid ISO 8601 date.
  isISO8601: function () {
    if (this.stopOnError()) return this
    if (!validator.isISO8601(input())) this.hasError = true
    return this
  },

  // check if the string is a valid ISO 3166-1 alpha-2 officially assigned country code.
  isISO31661Alpha2: function () {
    if (this.stopOnError()) return this
    if (!validator.isISO31661Alpha2(input())) this.hasError = true
    return this
  },

  // check if the string is a ISRC.
  isISRC: function () {
    if (this.stopOnError()) return this
    if (!validator.isISRC(input())) this.hasError = true
    return this
  },

  // TODO check if the string is in a array of allowed values.
  // isIn: function (values) {
  //   if (this.stopOnError()) return this
  //   if (!validator.isIn(input(), values)) this.hasError = true
  //   return this
  // },

  // check if the string is an integer.
  isInt: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isInt(String(input()), options)) this.hasError = true
    return this
  },

  // check if the string is valid JSON (note: uses JSON.parse).
  isJSON: function () {
    if (this.stopOnError()) return this
    if (!validator.isJSON(input())) this.hasError = true
    return this
  },

  // check if the string is a valid latitude-longitude coordinate in the format lat,long or lat, long.
  isLatLong: function () {
    if (this.stopOnError()) return this
    if (!validator.isLatLong(input())) this.hasError = true
    return this
  },

  // check if the string is lowercase.
  isLowercase: function () {
    if (this.stopOnError()) return this
    if (!validator.isLowercase(input())) this.hasError = true
    return this
  },

  // check if the string is a MAC address.
  isMACAddress: function () {
    if (this.stopOnError()) return this
    if (!validator.isMACAddress(input())) this.hasError = true
    return this
  },

  // check if the string is a MD5 hash.
  isMD5: function () {
    if (this.stopOnError()) return this
    if (!validator.isMD5(input())) this.hasError = true
    return this
  },

  // check if the string matches to a valid MIME type format.
  isMimeType: function () {
    if (this.stopOnError()) return this
    if (!validator.isMimeType(input())) this.hasError = true
    return this
  },

  // check if the string is a mobile phone number.
  isMobilePhone: function (locale) {
    if (this.stopOnError()) return this
    if (!validator.isMobilePhone(input(), locale)) this.hasError = true
    return this
  },

  // check if the string is a valid hex-encoded representation of a MongoDB ObjectId.
  isMongoId: function () {
    if (this.stopOnError()) return this
    if (!validator.isMongoId(input())) this.hasError = true
    return this
  },

  // check if the string contains one or more multibyte chars.
  isMultibyte: function () {
    if (this.stopOnError()) return this
    if (!validator.isMultibyte(input())) this.hasError = true
    return this
  },

  // check if the string is a valid port number.
  isPort: function () {
    if (this.stopOnError()) return this
    if (!validator.isPort(input())) this.hasError = true
    return this
  },

  // check if the string is a postal code.
  isPostalCode: function (locale) {
    if (this.stopOnError()) return this
    if (!validator.isPostalCode(input(), locale)) this.hasError = true
    return this
  },

  // check if the string is a postal code.
  isPostalCode: function (locale) {
    if (this.stopOnError()) return this
    if (!validator.isPostalCode(input(), locale)) this.hasError = true
    return this
  },

  // check if the string is a postal code.
  isSurrogatePair: function () {
    if (this.stopOnError()) return this
    if (!validator.isSurrogatePair(input())) this.hasError = true
    return this
  },

  // check if the string is an URL.
  isURL: function (options) {
    if (this.stopOnError()) return this
    if (!validator.isURL(input(), options)) this.hasError = true
    return this
  },

  // check if the string is a UUID (version 3, 4 or 5).
  isUUID: function (version) {
    if (this.stopOnError()) return this
    if (!validator.isUUID(input(), version)) this.hasError = true
    return this
  },

  // check if the string is a UUID (version 3, 4 or 5).
  isUppercase: function () {
    if (this.stopOnError()) return this
    if (!validator.isUppercase(input())) this.hasError = true
    return this
  },

  // check if the string contains a mixture of full and half-width chars.
  isVariableWidth: function () {
    if (this.stopOnError()) return this
    if (!validator.isVariableWidth(input())) this.hasError = true
    return this
  },

  // checks characters if they appear in the whitelist.
  isWhitelisted: function (chars) {
    if (this.stopOnError()) return this
    if (!validator.isWhitelisted(input(), chars)) this.hasError = true
    return this
  },

  // checks characters if they appear in the whitelist.
  matches: function (pattern, modifiers) {
    if (this.stopOnError()) return this
    if (!validator.matches(input(), pattern, modifiers)) this.hasError = true
    return this
  },

  // ----------------------------------------------

  // check if the string is a date that's before the specified date.
  isBool: function () { return this.isBoolean() },
  isBoolean: function () {
    if (this.stopOnError()) return this
    if (typeof input() !== 'boolean') this.hasError = true
    return this
  },

  // check if the string's length falls in a range.
  isLength: function (options) {
    if (this.stopOnError()) return this

    let min, max
    if (typeof (options) === 'object') {
      min = options.min || 0
      max = options.max
    } else { // backwards compatibility: isLength(str, min [, max])
      min = arguments[1]
      max = arguments[2]
    }

    const surrogatePairs = typeof input() === 'string' ? input().match(/[\uD800-\uDBFF][\uDC00-\uDFFF]/g) || [] : []
    const len = input().length - surrogatePairs.length

    if (!(len >= min && (typeof max === 'undefined' || len <= max))) {
      this.hasError = true
    }

    return this
  },

  // check if the input is in a array of allowed values.
  isIn: function (x) {
    if (this.stopOnError()) return this
    if (x.indexOf(input()) === -1) this.hasError = true
    return this
  },

  // check if the input is not a value.
  isNot: function (x) {
    if (this.stopOnError()) return this
    if (input() === x) this.hasError = true
    return this
  },

  // check if the input has not a length of zero.
  isNotEmpty: function () {
    if (this.stopOnError()) return this
    if (validator.isEmpty(input())) this.hasError = true
    return this
  },

  isNotNull: function () {
    if (this.stopOnError()) return this
    if (input() === null) this.hasError = true
    return this
  },

  isNotZero: function () {
    if (this.stopOnError()) return this
    if (input() === 0) this.hasError = true
    return this
  },

  isMaxLength: function (x) {
    if (this.stopOnError()) return this
    if (input().length > x) this.hasError = true
    return this
  },

  isMinLength: function (x) {
    if (this.stopOnError()) return this
    if (input().length < x) this.hasError = true
    return this
  },

  // check if the string contains only numbers.
  isNumber: function () { return this.isNumeric() },
  isNumeric: function () {
    if (this.stopOnError()) return this
    if (typeof input() !== 'number') this.hasError = true
    return this
  },

  isNull: function () {
    if (this.stopOnError()) return this
    if (input() !== null) this.hasError = true
    return this
  }

}

module.exports = Object.assign(assert, assertFunctions)
