# Assert.js

[![NPM version][npm-image]][npm-url] [![Downloads][downloads-image]][npm-url]

A library of asserts for Node.

## Installation and Usage

### Server-side usage

Install the library with `npm install assertjs`

#### No ES6

```javascript
var assert = require('assertjs')

assert(post)
.the('title').isNotEmpty().or('The post needs a title.')
.the('text').isNotEmpty().or('The post needs a text.')
.the('text').isMinLength(100).or('The text is too short.')

if (assert.hasError) {
	console.log(assert.error, assert.inputName)
}
```

#### ES6

```javascript
import assert from 'assertjs'
```

## Validators

Here is a list of the validators currently available.

Validator                               | Description
--------------------------------------- | --------------------------------------
***.contains(seed)***                   | check if the input contains the seed.
**.equals(comparison)**                 | check if the input matches the comparison.
**.isAfter([date])**                    | check if the input is a date that's after the specified date (defaults to now).
**.isAlpha([locale])**                  | check if the input contains only letters (a-zA-Z).<br/><br/>Locale is one of `['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'da-DK', 'de-DE', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sk-SK', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']`) and defaults to `en-US`.
**.isAlphanumeric([locale])**           | check if the input contains only letters and numbers.<br/><br/>Locale is one of `['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'da-DK', 'de-DE', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sk-SK', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']`) and defaults to `en-US`.
**.isAscii()**                          | check if the input contains ASCII chars only.
**.isBase64()**                         | check if a input is base64 encoded.
**.isBefore([date])**                   | check if the input is a date that's before the specified date.
**.isBoolean()**                        | check if a input is a boolean.
**.isByteLength(options)**              | check if the input's length (in UTF-8 bytes) falls in a range.<br/><br/>`options` is an object which defaults to `{min:0, max: undefined}`.
**.isCreditCard()**                     | check if the input is a credit card.
**.isCurrency(options)**                | check if the input is a valid currency amount.<br/><br/>`options` is an object which defaults to `{symbol: '$', require_symbol: false, allow_space_after_symbol: false, symbol_after_digits: false, allow_negatives: true, parens_for_negatives: false, negative_sign_before_digits: false, negative_sign_after_digits: false, allow_negative_sign_placeholder: false, thousands_separator: ',', decimal_separator: '.', allow_decimal: true, require_decimal: false, digits_after_decimal: [2], allow_space_after_digits: false}`.<br/>**Note:** The array `digits_after_decimal` is filled with the exact number of digits allowd not a range, for example a range 1 to 3 will be given as [1, 2, 3].
**.isDataURI()**                        | check if the input is a [data uri format](https://developer.mozilla.org/en-US/docs/Web/HTTP/data_URIs).
**.isDecimal(options)**                 | check if the input represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc.<br/><br/>`options` is an object which defaults to `{force_decimal: false, decimal_digits: '1,', locale: 'en-US'}`<br/><br/>`locale` determine the decimal separator and is one of `['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'da-DK', 'de-DE', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']`.<br/>**Note:** `decimal_digits` is given as a range like '1,3', a specific value like '3' or min like '1,'.
**.isDivisibleBy(number)**              | check if the input is a number that's divisible by another.
**.isEmail([options])**                 | check if the input is an email.<br/><br/>`options` is an object which defaults to `{ allow_display_name: false, require_display_name: false, allow_utf8_local_part: true, require_tld: true }`. If `allow_display_name` is set to true, the validator will also match `Display Name <email-address>`. If `require_display_name` is set to true, the validator will reject strings without the format `Display Name <email-address>`. If `allow_utf8_local_part` is set to false, the validator will not allow any non-English UTF8 character in email address' local part. If `require_tld` is set to false, e-mail addresses without having TLD in their domain will also be matched.
**.isEmpty()**                          | check if the input has a length of zero.
**.isFQDN([options])**                  | check if the input is a fully qualified domain name (e.g. domain.com).<br/><br/>`options` is an object which defaults to `{ require_tld: true, allow_underscores: false, allow_trailing_dot: false }`.
**.isFloat([options])**                 | check if the input is a float.<br/><br/>`options` is an object which can contain the keys `min`, `max`, `gt`, and/or `lt` to validate the float is within boundaries (e.g. `{ min: 7.22, max: 9.55 }`) it also has `locale` as an option.<br/><br/>`min` and `max` are equivalent to 'greater or equal' and 'less or equal', respectively while `gt` and `lt` are their strict counterparts.<br/><br/>`locale` determine the decimal separator and is one of `['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'da-DK', 'de-DE', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'it-IT', 'nb-NO', 'nl-NL', 'nn-NO', 'pl-PL', 'pt-BR', 'pt-PT', 'ru-RU', 'sr-RS', 'sr-RS@latin', 'sv-SE', 'tr-TR', 'uk-UA']`.
**.isFullWidth()**                      | check if the input contains any full-width chars.
**.isHalfWidth()**                      | check if the input contains any half-width chars.
**.isHash(algorithm)**                  | check if the input is a hash of type algorithm.<br/><br/>Algorithm is one of `['md4', 'md5', 'sha1', 'sha256', 'sha384', 'sha512', 'ripemd128', 'ripemd160', 'tiger128', 'tiger160', 'tiger192', 'crc32', 'crc32b']`
**.isHexColor()**                       | check if the input is a hexadecimal color.
**.isHexadecimal()**                    | check if the input is a hexadecimal number.
**.isIP([version])**                    | check if the input is an IP (version 4 or 6).
**.isISBN([version])**                  | check if the input is an ISBN (version 10 or 13).
**.isISSN([options])**                  | check if the input is an [ISSN](https://en.wikipedia.org/wiki/International_Standard_Serial_Number).<br/><br/>`options` is an object which defaults to `{ case_sensitive: false, require_hyphen: false }`. If `case_sensitive` is true, ISSNs with a lowercase `'x'` as the check digit are rejected.
**.isISIN()**                           | check if the input is an [ISIN][ISIN] (stock/security identifier).
**.isISO8601()**                        | check if the input is a valid [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date.
**.isISO31661Alpha2()**                 | check if the input is a valid [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) officially assigned country code.
**.isISRC()**                           | check if the input is a [ISRC](https://en.wikipedia.org/wiki/International_Standard_Recording_Code).
**.isIn(values)**                       | check if the input is in a array of allowed values.
**.isInt([options])**                   | check if the input is an integer.<br/><br/>`options` is an object which can contain the keys `min` and/or `max` to check the integer is within boundaries (e.g. `{ min: 10, max: 99 }`). `options` can also contain the key `allow_leading_zeroes`, which when set to false will disallow integer values with leading zeroes (e.g. `{ allow_leading_zeroes: false }`). Finally, `options` can contain the keys `gt` and/or `lt` which will enforce integers being greater than or less than, respectively, the value provided (e.g. `{gt: 1, lt: 4}` for a number between 1 and 4).
**.isJSON()**                           | check if the input is valid JSON (note: uses JSON.parse).
**.isLatLong()**                        | check if the input is a valid latitude-longitude coordinate in the format `lat,long` or `lat, long`.
**.isLength(options)**                  | check if the input's length falls in a range.<br/><br/>`options` is an object which defaults to `{min:0, max: undefined}`. Note: this function takes into account surrogate pairs.
**.isLowercase()**                      | check if the input is lowercase.
**.isMACAddress()**                     | check if the input is a MAC address.
**.isMD5()**                            | check if the input is a MD5 hash.
**.isMimeType()**                       | check if the input matches to a valid [MIME type](https://en.wikipedia.org/wiki/Media_type) format
**.isMobilePhone(locale)**              | check if the input is a mobile phone number,<br/><br/>(locale is one of `['ar-AE', 'ar-DZ','ar-EG', 'ar-JO', 'ar-SA', 'ar-SY', 'cs-CZ', 'de-DE', 'da-DK', 'el-GR', 'en-AU', 'en-CA', 'en-GB', 'en-HK', 'en-IN',  'en-KE', 'en-NG', 'en-NZ', 'en-RW', 'en-SG', 'en-UG', 'en-US', 'en-TZ', 'en-ZA', 'en-ZM', 'en-PK', 'es-ES', 'et-EE', 'fa-IR', 'fi-FI', 'fr-FR', 'he-IL', 'hu-HU', 'it-IT', 'ja-JP', 'ko-KR', 'lt-LT', 'ms-MY', 'nb-NO', 'nn-NO', 'pl-PL', 'pt-PT', 'ro-RO', 'ru-RU', 'sk-SK', 'sr-RS', 'tr-TR', 'uk-UA', 'vi-VN', 'zh-CN', 'zh-HK', 'zh-TW']` OR 'any'. If 'any' is used, function will check if any of the locales match).
**.isMongoId()**                        | check if the input is a valid hex-encoded representation of a [MongoDB ObjectId][mongoid].
**.isMultibyte()**                      | check if the input contains one or more multibyte chars.
**.isNot(value)**                       | check if the input is not a value.
**.isNotEmpty()**                       | check if the input is not empty.
**.isNotNull()**                        | check if the input is not null.
**.isNotZero()**                        | check if the input is not 0.
**.isNull()**                           | check if the input is null.
**.isNumeric()**                        | check if the input contains only numbers.
**.isMaxLength(max)**                   | check the maximum input's length.
**.isMinLength(min)**                   | check the minimum input's length.
**.isPort()**                           | check if the input is a valid port number.
**.isPostalCode(locale)**               | check if the input is a postal code,<br/><br/>(locale is one of `[ 'AT', 'AU', 'BE', 'CA', 'CH', 'CZ', 'DE', 'DK', 'DZ', 'ES', 'FI', 'FR', 'GB', 'GR', 'IL', 'IN', 'IS', 'IT', 'JP', 'KE', 'LI', 'MX', 'NL', 'NO', 'PL', 'PT', 'RO', 'RU', 'SA', 'SE', 'TW', 'US', 'ZA', 'ZM' ]` OR 'any'. If 'any' is used, function will check if any of the locals match).
**.isSurrogatePair()**                  | check if the input contains any surrogate pairs chars.
**.isURL([options])**                   | check if the input is an URL.<br/><br/>`options` is an object which defaults to `{ protocols: ['http','https','ftp'], require_tld: true, require_protocol: false, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false }`.
**.isUUID([version])**                  | check if the input is a UUID (version 3, 4 or 5).
**.isUppercase()**                      | check if the input is uppercase.
**.isVariableWidth()**                  | check if the input contains a mixture of full and half-width chars.
**.isWhitelisted(chars)**               | checks characters if they appear in the whitelist.
**.matches(pattern [, modifiers])**     | check if input matches the pattern.<br/><br/>Either `matches('foo', /foo/i)` or `matches('foo', 'foo', 'i')`.

## Contributing

In general, we follow the "fork-and-pull" Git workflow.

1. Fork the repo on GitHub
2. Clone the project to your own machine
3. Work on your fork
    1. Make your changes and additions
    2. Change or add tests if needed
    3. Run tests and make sure they pass
    4. Add changes to README.md if needed
4. Commit changes to your own branch
5. **Make sure** you merge the latest from "upstream" and resolve conflicts if there is any
6. Push your work back up to your fork
7. Submit a Pull request so that we can review your changes

[downloads-image]: http://img.shields.io/npm/dm/assertjs.svg

[npm-url]: https://npmjs.org/package/assertjs
[npm-image]: http://img.shields.io/npm/v/assertjs.svg

[mongoid]: http://docs.mongodb.org/manual/reference/object-id/
[ISIN]: https://en.wikipedia.org/wiki/International_Securities_Identification_Number
